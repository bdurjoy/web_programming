<?php
require "Model.php";
use Model as DB;

class mySQL extends DB{
    private $host = "localhost";
    private $userName = "root";
    private $password = "";
    private $dbName = "web_course";


    //variable for post information
    private $firstName;
    private $lastName;
    private $tNumber;
    private $email;
    private $socialNumber;
    private $paSS;
    private $date;
    //variable for database


    public function setData($allPostData=null)
    {

        if (array_key_exists("fName", $allPostData)) {
            $this->firstName = $allPostData['fName'];

        }
        if(array_key_exists("lName", $allPostData)){
            $this->lastName = $allPostData['lName'];
        }
        if(array_key_exists("tNumber", $allPostData)){
            $this->tNumber = $allPostData['tNumber'];
        }
        if(array_key_exists("eMail", $allPostData)){
            $this->email = $allPostData['eMail'];
        }
        if(array_key_exists("siN", $allPostData)){
            $this->socialNumber = $allPostData['siN'];
        }
        if(array_key_exists("passWord", $allPostData)){
            $this->paSS = $allPostData['passWord'];
        }

        $this->date = date("Y-m-d");
    }

    public function storeInfo(){
        $arrayData = array($this->firstName, $this->lastName,$this->email,
            $this->tNumber, $this->socialNumber,
            $this->paSS, $this->date);
        $insert = 'INSERT INTO employee (FirstName, LastName, EmailAddress, TelephoneNumber, SocialInsuranceNumber, Password, Date)
              VALUES (?, ?, ?, ?, ?, ?, ?)';

        $STH = $this->dbh->prepare($insert);
        $STH->execute($arrayData);
        header('Location: '."ViewAllEmployees.php");
    }
    public function displayData(){
        $sql = "Select * from employee";
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function checkInfo(){
        $sql = "Select * from employee WHERE EmailAddress=".mysqli_escape_string($this->email)." AND Password=$this->paSS";
        var_dump($sql);
        $STH=$this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_);
        return $STH->fetch();
    }

}
?>