<?php
include ("Header.php");
include "mySQL.php";
session_start();
if(!isset($_SESSION)){
    header('Location: '."Login.php");
}
//if(!isset($_SESSION)) {
//
//}
    $database = new mySQL();
    $allData = $database->displayData();



?>


    <!--banner section-->

    <div class="container container_menu">
        <?php include ("Menu.php");?>
        <div class="col-sm-8">
            <h2>Session State Data</h2>
            <p>First Name: <?php echo $_SESSION["firstName"]?></p>
            <p>Last Name: <?php echo $_SESSION["lastName"]?></p>
            <p>Email: <?php echo $_SESSION["email"]?></p>
            <p>Phone Number: <?php echo $_SESSION["phoneNum"]?></p>
            <p>SIN: <?php echo $_SESSION["sin"]?></p>
            <p>Password: <?php echo $_SESSION["pass"]?></p>
            <h2>Information</h2>
            <table>
                <tr>
                    <th class="col-sm-1">First Name</th>
                    <th class="col-sm-1">Last Name</th>
                    <th class="col-sm-2">Email Address</th>
                    <th class="col-sm-1">Phone Number</th>
                    <th class="col-sm-1">SIN</th>
                    <th class="col-sm-1">Password</th>
                </tr>
                <?php

                foreach($allData as $oneData){
                    ?>
                    <tr>
                        <td class="col-sm-1"><?php echo $oneData->FirstName?></td>
                        <td class="col-sm-1"><?php echo $oneData->LastName?></td>
                        <td class="col-sm-2"><?php echo $oneData->EmailAddress?></td>
                        <td class="col-sm-1"><?php echo $oneData->TelephoneNumber?></td>
                        <td class="col-sm-1"><?php echo $oneData->SocialInsuranceNumber?></td>
                        <td class="col-sm-1"><?php echo $oneData->Password?></td>
                    </tr>
                    <?php
                }
                ?>

            </table>
        </div>

    </div>

    <!--banner section finish -->

<?php
include ("Footer.php");
?>