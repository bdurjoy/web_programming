-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 02:53 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_course`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `EmployeeId` int(11) NOT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `LastName` varchar(50) DEFAULT NULL,
  `EmailAddress` varchar(255) DEFAULT NULL,
  `TelephoneNumber` varchar(20) DEFAULT NULL,
  `SocialInsuranceNumber` varchar(11) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`EmployeeId`, `FirstName`, `LastName`, `EmailAddress`, `TelephoneNumber`, `SocialInsuranceNumber`, `Password`, `Date`) VALUES
(1, 'durjoy', 'akash', 'akash@gmail.com', '646498798797', '21646', 'time', '2019-04-05'),
(2, 'akash', 'durjoy', 'type@gmail.com', '6464648', '249797', '', '2019-04-06'),
(3, 'durjoy', 'akash', 'type@gmail.com', '2479779', '294797', 'durjoy', '2019-04-06'),
(4, 'durjoy', 'akash', 'ajflja@gmail.com', '972497', '249797', 'team', '2019-04-06'),
(5, 'akash', 'durjoy', 'pirate@gmail.com', '294979797', '49797', 'typical', '2019-04-06'),
(6, 'dave', 'john', 'kiron@yahoo.ca', '42979797', '2499797858', 'bolt', '2019-04-06'),
(7, 'dave', 'john', 'brent@yahoo.ca', '42979797', '2499797858', 'told', '2019-04-06'),
(8, 'type', 'durjoy', 'dive@inbox.ru', '1479797', '24977', 'york', '2019-04-06'),
(9, 'tim', 'cook', 'kite@yo.ca', '439975', '998581', 'pant', '2019-04-06'),
(10, 'ben', 'fike', '', '29555', '', '', '2019-04-06'),
(11, 'kevein', '', '', '', '', '', '2019-04-06'),
(12, 'any', 'brian', 'quick@cut.ca', '9598888', '94898488', 'aksh', '2019-04-06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`EmployeeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `EmployeeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
