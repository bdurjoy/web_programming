<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale=1">
    <title>Plexuslog.com</title>
    <link href="resources/stylesheet/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <!--header section -->
    <header>
        <div class="wrapper wrapper_header">
            <div class="title">
                <h2>Plexuslog.com</h2>
            </div>

            <div class="nav_bar">
                <nav>
                    <ul>
                        <?php
                        echo "
                        <li><a href=\"#\" class=\"active\">Home</a></li>
                        <li><a href=\"../Lab1/index.html\">Lab1</a></li>
                        <li><a href=\"../Lab2/index.html\">Lab2</a></li>
                        <li><a href=\"../Lab3/index.html\">Lab3</a></li>
                        <li><a href=\"../Lab4/index.html\">Lab4</a></li>
                        <li><a href=\"../Assignment1/index.html\">Assignment1</a></li>
                        ";
                        ?>
                    </ul>
                </nav>
            </div>

        </div>
    </header>
    <!--header section finsih-->
    <!--banner section-->
    <menu class="wrapper banner banner_index">
        <?php
            $helloWorld = "Hello World!";
            echo "<h1>".$helloWorld." I am using PHP!</h1>";
            echo "<h3>Time: ";
            echo date("h:i:s");
            echo "</h3>";
        ?>
    </menu>

    <!--banner section finish -->
    <!--footer section-->
    <footer>
        <div class="wrapper wrapper_footer">
            <?php
            $firstName = "Durjoy";
            $lastName = "Barua";
            echo "<h3>Name: ".$firstName." "."$lastName</h3>
            <h3>Student ID: 040919612</h3>";
            ?>

            <h3><a href="mailto: baru0009@algonquinlive.com">Email: baru0009@algonquinlive.com</a> </h3>
            <hr>
            <p>&copy; Copyright: Plexuslog.com & Prexela.com, 2019</p>
        </div>
    </footer>
    <!--footer section finish -->
</body>
</html>