<?php

require "EventItem.php";
class class_lib extends EventItem{

   //private $itemname = "steak";
   //private $desc = "Fresh flame broiled AAA Angus beef done to perfection.";
   //private $price = "$49.99";

   function __construct($param1, $param2, $param3, $param4)
   {
      parent::__construct($param1, $param2, $param3, $param4);
   }

   function get_eventName() {

      return parent::geteventName();
   }

   function get_eventDate() {
   
   	return parent::geteventDate();
   }
   
   function get_eventDesc() {

      return parent::geteventDesc();
   }

   function get_eventPrice() {

      return parent::geteventPrice();
   }
}

?>

