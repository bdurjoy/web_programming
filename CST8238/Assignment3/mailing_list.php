<?php
/**
 * Created by PhpStorm.
 * User: Durjoy
 * Date: 4/17/2019
 * Time: 10:27 PM
 */

include "header.php";
include "class_lib.php";
include "MailSql.php";

$database = new MailSql();
$list = $database->view();
?>



<div id="content" class="clearfix">

    <table>
        <tr>
            <th>Full Name</th>
            <th>Email Address</th>
            <th>Email Hash</th>
            <th>Phone</th>
        </tr>
        <?php
            foreach($list as $eachData){
                echo "<tr>";
                ?>
                <td><?php echo $eachData->firstName." ".$eachData->lastName ?></td>
                <td><?php echo $eachData->emailAddress?></td>
                <td><?php echo $eachData->emailHash?></td>
                <td><?php echo $eachData->phoneNumber?></td>

        <?php

                echo "<tr>";
            }
        ?>
    </table>
</div>


<?php

include "footer.php";
?>
