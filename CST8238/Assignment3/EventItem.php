<?php

/**
 * Created by PhpStorm.
 * User: Durjoy
 * Date: 4/17/2019
 * Time: 11:34 AM
 */
class EventItem
{
    private $eventName = "";
    private $eventDate = "";
    private $eventDesc = "";
    private $eventPrice = "";

    function __construct($param1, $param2, $param3, $param4) {
        $this->eventName = $param1;
        $this->eventDate = $param2;
        $this->eventDesc = $param3;
        $this->eventPrice = $param4;
    }

    public function geteventName(){
        return $this->eventName;
    }

    public function geteventDate(){
        return $this->eventDate;
    }

    public function geteventDesc(){
        return $this->eventDesc;
    }

    public function geteventPrice(){
        return $this->eventPrice;
    }

}

?>