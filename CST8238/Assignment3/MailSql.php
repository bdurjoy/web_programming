<?php

/**
 * Created by PhpStorm.
 * User: Durjoy
 * Date: 4/17/2019
 * Time: 9:23 PM
 */
require "Model.php";
use Model as DB;


class MailSql extends DB{
    private $fName ;
    private $lName;
    private $phoneN;
    private $eMail;
    private $userName;
    private $reFerral;
    private $eMailHash;
    private $message;
    private $passW;


    public function setData($allPostData = null){
        if (array_key_exists("customerfName", $allPostData)) {
            $this->fName = $_POST["customerfName"];
        }

        if(array_key_exists("customerlName", $allPostData)){
            $this->lName = $_POST["customerlName"];
        }

        if(array_key_exists("phoneNumber", $allPostData)){
            $this->phoneN = $_POST["phoneNumber"];
        }
        if(array_key_exists("emailAddress", $allPostData)) {
            $this->eMail = $_POST["emailAddress"];
            $this->eMailHash = md5($_POST["emailAddress"]);
        }
        if(array_key_exists("username", $allPostData)) {
            $this->userName = $_POST["username"];
        }
        if(array_key_exists("referral", $allPostData)) {
            $this->reFerral = $_POST["referral"];
        }

        if(array_key_exists("passWord", $allPostData)){
            $this->passW = $_POST["passWord"];
        }
    }

    public function mailStore(){
        $arrayData = array($this->fName, $this->lName, $this->phoneN, $this->eMail, $this->eMailHash, $this->userName, $this->reFerral);
        $insert = 'INSERT INTO mailinglist (firstName, lastName, phoneNumber, emailAddress, emailHash, userName, referrer)
              VALUES (?, ?, ?, ?, ?, ?, ?)';

        $STH = $this->dbh->prepare($insert);
        $result = $STH->execute($arrayData);


        if($result){
            echo "Data Inserted in the database successfully";
        }else{
            echo "Data could not inserted in the database";
        }
    }

    public function view(){
        $view = 'SELECT * FROM mailinglist';
        $STH = $this->dbh->query($view);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function login(){
        $login = "SELECT * FROM adminusers WHERE Username='$this->userName' AND Password='$this->passW' LIMIT 1";
        $STH = $this->dbh->query($login);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $result = $STH->fetch();

        if($result){
            $currentDate = "UPDATE adminusers SET Lastlogin=? WHERE Username='$this->userName' AND Password='$this->passW' LIMIT 1";
            $update = $this->dbh->prepare($currentDate);
            $update->execute(array(date("Y-m-d")));
            $_SESSION['user'] = "Admin";
            
            header('Location: '."internal.php");
        }else{
            echo "Could not be verified";
        }
    }

}