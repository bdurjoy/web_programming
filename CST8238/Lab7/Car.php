<?php
/**
 * Created by PhpStorm.
 * User: Durjoy
 * Date: 3/7/2019
 * Time: 11:56 AM
 */

include "LandVehicle.php";

class Car extends LandVehicle{
    private $speedLimit;


    public function __construct($make, $model, $year, $price, $speedLimit)
{
        parent::__construct($make,$model, $year,$price);
        $this->speedLimit = $speedLimit;
    }

    public function displayVehicleInfo(){
        parent::displayVehicleInfo();
        echo ", <strong>Speed Limit: </strong>$this->speedLimit";
    }
}
?>