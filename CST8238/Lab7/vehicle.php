<?php
include ("Header.php");
include ("Menu.php");

interface Vehicle{
    public function displayVehicleInfo();
}

include "Car.php";
include "Boat.php";
?>

<!--banner section-->

<div class="banner">
    <?php
        $objCar1 = new Car("Toyota", "Camry", 1992, 2000, 180);
        $objCar2 = new Car("Honda", "Accord", 2002, 6000, 200);

        $objBoat1 = new Boat("Mitsubishi", "Tubo", 1999, 20000, 18);
        $objBoat2 = new Boat("Hyundai", "XT", 2012, 26000, 8);

        echo "<h2>Car</h2>";
        $objCar1->displayVehicleInfo();
        echo "<br>";
        $objCar2->displayVehicleInfo();

        echo "<h2>Boat</h2>";
        $objBoat1->displayVehicleInfo();
        echo "<br>";
        $objBoat2->displayVehicleInfo();




    ?>
</div>
</menu>
<!--banner section finish -->

<?php
include ("Footer.php");
?>