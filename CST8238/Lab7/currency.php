<?php
include ("Header.php");
include ("Menu.php");
//currency declaration

    $currencies = array("CAD" => "Canadian Dollar",
        "NZD" => "New Zealand Dollar",
        "USD" => "US Dollar");

    $rates = array("CAD" => 0.97645,
        "NZD" => 1.20642,
        "USD" => 1.0);
?>
    <!--banner section-->

    <section class=" banner banner_index">
        <h4>


            <form action="currency.php" method="post">
                <p>Covert</p>
                <input name="srcamt" placeholder="Amount">
                <select name="basecurr">
                    <option value="CAD">Canadian Dollar</option>
                    <option value="NZD">New Zealand Dollar</option>
                    <option value="USD">US Dollar</option>
                </select>

                <h5>to</h5>
                <select name="destcurr">
                    <option value="CAD">Canadian Dollar</option>
                    <option value="NZD">New Zealand Dollar</option>
                    <option value="USD">US Dollar</option>
                </select>
                <br><br>
                <input type="submit">
            </form>

            <?php
            if(!empty($_POST)) {
                $amount_input = $_POST["srcamt"];

                $converted_output = ($amount_input/$rates[$_POST["basecurr"]])*$rates[$_POST["destcurr"]];


                echo $amount_input." ".$currencies[$_POST["basecurr"]]." converts to ".$converted_output." ".$currencies[$_POST["destcurr"]];
            }
            ?>
        </h4>
    </section>
    </menu>
    <!--banner section finish -->
<?php
include ("Footer.php");
?>