<?php
include ("Header.php");
include ("Menu.php");
?>
    <!--banner section-->

    <section class=" banner banner_index">
            <?php
            $Printers = array(
                array("Epson", 100, 2500),
                array("Cannon", 100, 3000),
                array("Xerox", 500, 2000),
            );

            $Laptops = array(
                array("Apple", 200, 2000),
                array("HP", 300, 1500),
                array("Toshiba", 100, 1200),
                array("Asus", 200, 1900)
            );

            $TVs = array(
                array("Samsung", 500, 1200),
                array("LG", 500, 1050),
                array("Sony", 200, 1000)
            );

            $Products = array(0 => $Printers, 1 => $Laptops, 2=> $TVs);

            ?>
            <table>
                <tr>
                    <th>Category</th>
                    <th>Brand</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>


            <?php
            foreach($Products as $product) {

                if($product == $Products[0]){
                    $prod_name= "Printer";
                }elseif($product == $Products[1]){
                    $prod_name = "Laptops";
                }elseif($product == $Products[2]){
                    $prod_name = "TV";
                }else{
                    $prod_name = " ";
                }
                for ($gadget_count = 0; $gadget_count < count($product); $gadget_count++) {
                    echo "<tr><td>";
                    if($gadget_count == 0) {
                        echo "$prod_name";
                    }
                    echo "</td>";
                    for ($i = 0; $i < 3; $i++) {

                        echo "<td>" . $product[$gadget_count][$i] . "</td>";
                    }
                    echo "</tr>";
                }
            }
            // echo "<td>".$tag.": ".$Products["Printer"][$print_count][$i]."</td>";

            //            $Products = array("Printer" =>
//                array(
//                "Brand" => array("Epson", "Cannon", "Xerox")
//            ));


            ?>
            </table>
    </section>
    </menu>
    <!--banner section finish -->
<?php
include ("Footer.php");
?>