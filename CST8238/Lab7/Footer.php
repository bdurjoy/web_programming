


<script>
    // Add active class to the current button (highlight it)
    var header = document.getElementById("list");
    var links = header.getElementById("link");
    for (var i = 0; i < links.length; i++) {
        links[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            if (current.length > 0) {
                current[0].className = current[0].className.replace(" active", "");
            }
            this.className += " active";
        });
    }
</script>

<!--footer section-->
<footer>
    <div class="wrapper wrapper_footer">
        <h3>Name: Durjoy Barua</h3>
        <h3>Student ID: 040919612</h3>
        <h3><a href="mailto: baru0009@algonquinlive.com">Email: baru0009@algonquinlive.com</a> </h3>
        <hr>
        <p>&copy; Copyright: Plexuslog.com & Prexela.com, 2019</p>
    </div>
</footer>
<!--footer section finish -->
</body>
</html>