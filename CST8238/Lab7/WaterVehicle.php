<?php

/**
 * Created by PhpStorm.
 * User: Durjoy
 * Date: 3/7/2019
 * Time: 12:37 PM
 */


class WaterVehicle implements Vehicle
{
    protected $make;
    protected $model;
    protected $year;
    protected $price;


    public function __construct($make, $model, $year, $price)
    {
        $this->make = $make;
        $this->model = $model;
        $this->year = $year;
        $this->price = $price;
    }

    public function displayVehicleInfo()
    {
        echo "<strong>Make</strong>: $this->make, ";
        echo "<strong>Model</strong>: $this->model, ";
        echo "<strong>Year</strong>: $this->year, ";
        echo "<strong>Price</strong>: $this->price";
    }

}

?>