<?php
include ("Header.php");
include ("Menu.php");
?>
    <!--banner section-->

    <section class=" banner banner_index">
        <h4>

            <form action="NameForm.php" method="post">
                First Name: <input type="text" name="first_name"><br><br>
                Middle Name: <input type="text" name="middle_name"><br><br>
                Last Name: <input type="text" name="last_name"><br><br>
            <input type="submit"> <a href="NameForm.php"> <input type="button" value="Reset"></a>
            </form>

            <?php
            if(!empty($_POST)) {
                if ($_POST["first_name"]) {
                    $firstName = $_POST["first_name"];
                } else {
                    $firstName = false;
                }
                if ($_POST["middle_name"]) {
                    $middleName = $_POST["middle_name"];
                } else {
                    $middleName = false;
                }
                if ($_POST["last_name"]) {
                    $lastName = $_POST["last_name"];
                } else {
                    $lastName = false;
                }
                $time = date('H');
                if ($time < 12) {
                    $message = "Good Morning";
                } else {
                    $message = "Good Day";
                }
                if (!$firstName && !$middleName && !$lastName) {
                    echo "You did not supply any names";
                } elseif ($firstName && !$middleName && !$lastName) {
                    echo "$message $firstName! You did not provide any Middlename and LastName";
                } elseif ($firstName && $middleName && !$lastName) {
                    echo "$message $firstName $middleName! You did not provide any LastName";
                } elseif ($firstName && !$middleName && $lastName) {
                    echo "$message $firstName $lastName! You did not provide any MiddleName.";
                } elseif ($firstName && $middleName && $lastName) {
                    echo "$message $firstName $middleName $lastName! Your middle name is very unique.";
                } else {
                    echo "$message, Welcome to the world of PHP!";
                }
            }
            ?>
        </h4>
    </section>
    </menu>
    <!--banner section finish -->
<?php
include ("Footer.php");
?>