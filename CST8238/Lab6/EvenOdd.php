<?php
include ("Header.php");
include ("Menu.php");
?>
    <!--banner section-->

    <section class=" banner banner_index">
        <h4>
            <?php
            $bottleBear = 99;

            for(; $bottleBear >=1; $bottleBear--){
                if($bottleBear%2 == 0){
                    $guest = "even number of guests";
                }elseif($bottleBear == 1){
                    $guest = "Only One guest";
                }else{
                    $guest = "odd number of guests";
                }

                echo "$bottleBear bottles of bear can serve $guest<br>";

            }

            ?>
        </h4>
    </section>
    </menu>
    <!--banner section finish -->
<?php
include ("Footer.php");
?>